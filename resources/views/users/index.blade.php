@extends('base')
@section('content')
<div class="uk-flex uk-flex-between">
	<div>
		<h3>Пользователи</h3>
	</div>
	<div>
		<ul class="uk-iconnav">
			<li><a href="" uk-icon="plus"></a></li>
			<li><a href="" uk-icon="file-edit"></a></li>
			<li><a href="" uk-icon="trash"></a></li>
		</ul>
	</div>
</div>
<div class="uk-padding-small tm-background-content">
	<table class="uk-table uk-table-middle uk-table-hover uk-table-striped uk-text-small">
		<thead>
			<th style="width: 20px;"><span uk-icon="move"></span></th>
			<th class="uk-table-shrink">ID</th>
			<th>Имя</th>
		</thead>
		<tbody uk-sortable="handle: .uk-sortable-handle" id="sort" class="js-filter">
			@foreach($users as $user)
			<tr id="{{ $user->id }}" data-id="{{ $user->id }}">
				<td class="uk-sortable-handle"><span uk-icon="grid"></span></td>
				<td>{{ $user->id }}</td>
				<td>{{ $user->name }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection
@section('js')
<script>
	document.addEventListener("moved", function() {
	let parent = document.querySelector('#sort');
	let elems = parent.children;
	let arr = []
	for (let elem of elems) {
		arr.push(elem.id)
	}
		console.log(arr);
	    htmx.ajax("post", "http://docu.test", {values: arr, swap: 'none'});
	});
	document.addEventListener("sort", function() {
		UIkit.notification("Список перестроен", {pos: 'top-center', status: 'primary', timeout: 800})
	})
</script>
@endsection
</body>
</html>