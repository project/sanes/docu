<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Users</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.21.13/dist/css/uikit.min.css" />

	<script src="https://cdn.jsdelivr.net/npm/uikit@3.21.13/dist/js/uikit.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.21.13/dist/js/uikit-icons.min.js"></script>	
	<script src="https://unpkg.com/htmx.org@2.0.2"></script>
	<style>
		.uk-notification-message {font-size: 1rem; border: solid 1px #eee; background-color: #fff;}
		.tm-background-content {background-color: #fff;}
	</style>
</head>
<body hx-headers='{"X-CSRF-TOKEN": "{{ csrf_token() }}"}' hx-boost="true" class="uk-background-muted">
	<div class="uk-container">
		@include('navbar')
		<div class="uk-grid-small" uk-grid>
			<div class="uk-width-1-5@l uk-width-1-4@m uk-visible@m">
				@include('sidebar')
			</div>
			<div class="uk-width-expand">
				@yield('content')
			</div>
		</div>
	</div>
	@yield('js')
</body>
</html>