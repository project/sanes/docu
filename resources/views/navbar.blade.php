<div class="uk-navbar" uk-navbar>
	<div class="uk-navbar-left">
		<a href="{{ config('app.url') }}" class="uk-navbar-item uk-logo uk-text-uppercase" hx-boost="false">{{ config('app.name') }}</a>
	</div>
</div>
<hr class="uk-margin-remove-top">